# FreedomBox Cloud Image Builder

Build images of FreedomBox for various cloud providers.

## Requirements:
- ansible and its dependencies

## Supported cloud service providers:
- AWS EC2

