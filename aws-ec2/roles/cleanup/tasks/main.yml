---
# Delete all running ec2 instances i.e. builders with failed builds

- name: Fetch all ec2 instances in "{{ var_region }}" region
  ec2_instance_facts:
    region: "{{ var_region }}"
  register: builders

- set_fact:
    instance_ids: "{{ builders.instances | selectattr('key_name', 'equalto', 'demo') | map(attribute='instance_id') | list }}"

- name: log instance_ids
  debug: msg="{{ instance_ids }}"

- name: Delete all builder instances
  ec2:
    key_name: demo
    region: eu-central-1
    state: absent
    instance_ids: "{{ instance_ids }}"
  when: "{{ instance_ids }}"


# Delete all but the latest AMI

- name: gather facts about the AMIs in "{{ var_region }}" region
  ec2_ami_facts:
    region: "{{ var_region }}"
    filters:
      "tag:Name": FreedomBox
  register: ami_facts
  
- set_fact:
    all_images: "{{ ami_facts.images|sort(attribute='creation_date') }}"

- set_fact:
    images_to_delete: "{{ all_images[:-1] }}"

- name: log images to delete
  debug: msg="{{ images_to_delete }}"

- name: Deregister and delete all older AMIs
  ec2_ami:
    image_id: "{{ item.image_id }}"
    region: "{{ var_region }}"
    delete_snapshot: True
    state: absent
  with_items:
    "{{ images_to_delete }}"


# Delete any old left-over snapshots.
# Deleting AMIs should delete their associated snapshots too.
# This is an additional cleanup step to delete snapshots created during development.

# FIXME This might delete the snapshot of the demo server as well
- name: Gather old snapshots in "{{ var_region }}" region
  ec2_snapshot_facts:
    region: "{{ var_region }}"
    owner_ids:
      - "{{ user_id }}"
  register: snapshot_response

- set_fact:
    snapshots: "{{ snapshot_response.snapshots|sort(attribute='start_time') }}"

- set_fact:
    snapshots_to_delete: "{{ snapshots[:-1] }}"

# Some snapshots might have AMIs associated with them and will throw errors.
# The errors are being ignored here.
# You should be suspicious if the errors are not at the end,
# since snapshots is a sorted list
- local_action:
    module: ec2_snapshot
    region: "{{ var_region }}"
    snapshot_id: "{{ item.snapshot_id }}"
    state: absent
  with_items:
    "{{ snapshots_to_delete }}"
  when: "{{ snapshots_to_delete }}"
  ignore_errors: True
