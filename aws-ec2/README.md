# Ansible playbooks for AWS EC2

This module contains the required Ansible playbooks and bootstrap-vz configuration for building Amazon Machine Images (AMIs) for AWS EC2.

## Setup

Install Ansible and libraries for AWS EC2 on the machine which executes these playbooks.

```
$ sudo apt install ansible python3-boto3 python3-boto
```

You have to obtain a key file for programmatic access to AWS from the AWS console and provide the path to it in /etc/ansible/ansible.cfg. A sample file containing the required parameters is provided in this directory as ansible.cfg.sample.

You should also specify your AWS access key and secret key in a file called secret_vars.yml. Copy secret_vars.yml.sample into a new file secret_vars.yml and change the values.

Run the following command to generate a new FreedomBox AMI.

```
$ ansible-playbook ec2-build-AMI.yml
```

## Roles

### cleanup
This role cleans up any left-over AMIs, snapshots, volumes and instances running after creating the FreedomBox AMI. This role can be invoked by calling its corresponding playbook.

```
$ ansible-playbook cleanup.yml
```

### demo
This role replaces the FreedomBox demo server with a new one. This can be invoked as follows:

```
$ ansible-playbook demo-reset.yml
```
